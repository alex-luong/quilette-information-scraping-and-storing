#!/bin/bash

# Delete all unneeded dependencies from lambda code to minimise size

# Unzip the output lambda code
# Delete all unneeded files using lambdaDep.txt as del list
# Repack the zip with new files done
fileName=lambda-dydb-1.0-SNAPSHOT-lambda_deployment_package_assembly.zip
delListFile=~/Desktop/projects/java/dynamodb_test/lambda-dydb/lambdaDep.txt
targetPath=~/Desktop/projects/java/dynamodb_test/lambda-dydb/target/

cd target/

#Unzip files
unzip $fileName -d lambda_unzipped
rm $fileName

cd lambda_unzipped/lib

# Read all lines in the lambdaDep file
while read line 
do
  # Delete all files read from the file
  rm $line
done < $delListFile

cd ../

#Rezip file without all the dependencies
zip -r $targetPath$fileName lib/ com/

cd ../
#Delete folder
rm -rf lambda_unzipped

#Add part to upload to AWS S3? 