package com.rvm.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;

public class DyQuery {
  private AmazonDynamoDB client;
  private DynamoDB dynamoDB;
  private String tName;

  // Getters
  public AmazonDynamoDB getAWSClient() { 
    return client;
  }
  public DynamoDB getDyClient() { 
    return dynamoDB;
  }
  public String getTName() { 
    return tName;
  }

  // Setters
  public void setAWSClient(AmazonDynamoDB awsClient) { 
    this.client = awsClient;
  }
  public void setDyClient(DynamoDB dyDB) { 
    this.dynamoDB = dyDB;
  }
  public void setTName(String tableName) { 
    this.tName = tableName;
  }

  // Class Initiailsers 
  public DyQuery() { 
  }
  public DyQuery(AmazonDynamoDB client, DynamoDB dynamoDB, String tableName) { 
    this.client = client;
    this.dynamoDB = dynamoDB;
    this.tName = tableName;
  }

  // Private Funcs

  // Convert Month day, year string into epoch string
  private String convertDateEpoch(String date) { 
    long epoch = 0;

    try { 
      // Format the inputted date string
      SimpleDateFormat newDateFormat = new SimpleDateFormat("MMMM dd, yyyy");
      // Parse it into date format and convert to epoch
      Date theDate = newDateFormat.parse(date);
      epoch = theDate.getTime();

    } catch(Exception e) { 
      System.out.println("Date error: " + e.toString());
    }

    return Long.toString(epoch);
  }

  // Delete items from DB
  private void deleteDBItems(String tName, List<Map<String, AttributeValue>> itemList) { 
    DescribeTableResult tableRes = client.describeTable(tName);
    String partitionKey = tableRes.getTable().getKeySchema().get(0).getAttributeName();

    System.out.println("ItemsCount to del: " + itemList.size());

    // Delete all items in the list, 1 by 1
    for( Map<String, AttributeValue> itemsDel : itemList) { 
      try { 
        // Del key needs partition key name and value
        Map<String, AttributeValue> delKey = new HashMap<String, AttributeValue>();
        delKey.put(partitionKey, new AttributeValue(itemsDel.get(partitionKey).getS()));

        DeleteItemRequest delItemReq = new DeleteItemRequest(tName, delKey);
        client.deleteItem(delItemReq);

      } catch(Exception e) { 
        System.out.println("Cannot del item: " + e.toString());
      }
    }

    System.out.println("Successfully deleted the items");
  }

  // Public Funcs

   // Callback function to add a single entry to a table (tname = TableName)
  public void popDataTable(String tName, String date, String author, String articleLink, String imgLink, String summary) {
    date = convertDateEpoch(date);
    
    // Create fields to add into table
    Map<String, AttributeValue> tableEntry = new HashMap<String, AttributeValue>();
    tableEntry.put("Date", new AttributeValue().withN(date));
    tableEntry.put("Author", new AttributeValue(author));
    tableEntry.put("Article_Link", new AttributeValue(articleLink));
    tableEntry.put("ImgLink", new AttributeValue(imgLink));
    tableEntry.put("Summary", new AttributeValue(summary));
    tableEntry.put("Dummy_Partition", new AttributeValue("Dummy"));

    try {
      System.out.println("Adding a new item...");
      client.putItem(tName, tableEntry);

      System.out.println("Item Added");
    }
    catch(Exception e) { 
      System.out.println("Error: " + e);
    }
  }

  // Maintain DB, check if size > 100, delete all items after that
  public void maintainDB(String tName, String paritionName) {
    DescribeTableResult tableRes = client.describeTable(tName);
    String gsi = tableRes.getTable().getGlobalSecondaryIndexes().get(0).getIndexName();
    long tableSize = tableRes.getTable().getItemCount();

    System.out.println("TableItemCount: " + Long.toString(tableSize));
    // Get the 100's LastKeyEvaulated and then delete everything from there on
    if( tableSize > 100) {
      // Assign #name to a string
      HashMap<String, String> partitionNames = new HashMap<String,String>();
      partitionNames.put("#parition", paritionName);

      // Assign :name to an AttributeValue
      HashMap<String, AttributeValue> partitionMap = new HashMap<String,AttributeValue>();
      partitionMap.put(":paritionVal", new AttributeValue().withS("Dummy"));

      QueryRequest queryTable = new QueryRequest() 
        .withTableName(tName)
        .withIndexName(gsi)
        .withLimit(100)  // Limits number of items to check
        .withScanIndexForward(false)  // Output based on sorted key in descending order
        .withExpressionAttributeNames(partitionNames) 
        .withExpressionAttributeValues(partitionMap)
        .withKeyConditionExpression("#parition  = :paritionVal"); // Equality Check (Must be included)

      // LastEvalKey is 100th element in DB
      QueryResult itemQuery = client.query(queryTable);
      Map<String, AttributeValue> lastEvalKey = itemQuery.getLastEvaluatedKey();

      // Set ExclusiveStartKey and get all items to delete
      queryTable.setExclusiveStartKey(lastEvalKey);
      itemQuery = client.query(queryTable);
      
      // Delete items
      deleteDBItems(tName, itemQuery.getItems());
    }

    System.out.println("All older items past the 100th element has been deleted");
  }
  

}