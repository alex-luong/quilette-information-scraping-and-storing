package com.rvm.app;

import java.io.InputStream;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

/**
 * Set up a Lambda Handler to webscrape from a site "Quilette" and add those entries in the db
 * Also maintain the DB, checking when the db.count > 100 items and delete the oldest entries
 */
public class Lambda_DyDBTrigger {
	// Can either use inputstreams, str, int, or classes (POJO format)
	public void appHandler(InputStream instr, Context context) { 
		LambdaLogger logger = context.getLogger();
		String tableName = "Qui";
		String siteUrl = "";
		String partitionKey = "Article_Link";
		String sortKey = "Date";
		
		logger.log("Connecting into aws client");
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
		.withRegion(Regions.AP_SOUTHEAST_1)
		.build();
		DynamoDB dynamoDB = new DynamoDB(client);

		// Initailise class objs
		DyQuery dyDB = new DyQuery(client, dynamoDB, tableName);
		WebScrape webScrapper = new WebScrape(dyDB);
		logger.log("Initalise class obsj");

		// Webscrape site and output into DynamoDB
		webScrapper.scrapSite(siteUrl);

		// Maintain DB
		dyDB.maintainDB(tableName, partitionKey);

		logger.log("Successfully webscrapped a site and added info into dynamodb");
	}
}
