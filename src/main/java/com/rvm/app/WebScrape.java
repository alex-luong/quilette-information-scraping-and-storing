package com.rvm.app;

// Jsoup modules
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/** This class connects to a website and webscrapes all the front page, taking in the Author, title, summary, link and link and
 * outputting the results directly to a Dynamod database 
*
*/
public class WebScrape { 
  private DyQuery dydb;

  // Getters

  public DyQuery getDyDB() { 
    return dydb;
  }

  // Setters

  public void setDyObj(DyQuery db) { 
    this.dydb = db;
  }

  // Initalise Classes
  public WebScrape() { 

  }
  public WebScrape(DyQuery dynadb) { 
    this.dydb = dynadb;
  }

  // Public Funcs

  // Connect to HTML site with JSoup and webscrapes required information
  public void scrapSite(String url) { 
    String tName = dydb.getTName();
    Document doc;

    try { 
      // Connect to URL and get the HTML page/parse it
      doc = Jsoup.connect(url).get();

      // Get DOM elements and add to List String
      Elements topStories = doc.select("#primary .category-top-stories");
      for( int i= 0; i< topStories.size(); i++) {
        // Get Author, article link, img link and summary text
        Elements articleLink = topStories.get(i).select(".entry-header .entry-title a[href]");
        Elements author = topStories.get(i).select(".entry-header .entry-details a[href]");
        Elements img = topStories.get(i).select(".entry-thumbnail img[src]");
        Elements summary = topStories.get(i).select(".entry-summary");
        Elements date = topStories.get(i).select(".entry-date a[href]");

        // Add the date, author, article link, imglink and article summary into the preassigned table
        dydb.popDataTable(tName, date.text(), author.text(), articleLink.attr("href"), img.attr("src"), summary.text());
      }
    } catch(Exception e) { 
      System.out.println("WebScraping error: " + e.toString());
    }
  }
}