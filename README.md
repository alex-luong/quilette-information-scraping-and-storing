# Automated Webscaper, outputting results in AWS DyDB

**Purpose**
    This is an automated webscraper that would take the title, author, link and date of the front page of Quiliette and output the scrapped contents into a AWS DynamodDB

    This is designed to be run via AWS Lambda, so that the program would automatically scrap the front page of 
    Quilette each day and also maintain the database, by ensuring there's at most 100 items. 

**Running Program**
    Run unzip_del_dep_rezip.sh in terminal after adding execution permission

    This would automatically convert the java into a JAR, adding necessary dependancies to create an UberJar


